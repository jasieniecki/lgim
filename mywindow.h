// The header file of MyWindow class
// The object of this class is the main window of our application
// The skeleton of this file is created by the Qt Creator
// You can add here your own fields and methods

#ifndef MYWINDOW_H
#define MYWINDOW_H

// MyWindow class inherits from class QMainWindow
// Class QMainWindow has its own layout.
// You can easily add to it the menu bar, toolbar and status bar.
// In the middle of the window displayed by QMainWindow
// is located area that you can fill with different widgets.
#include <QMainWindow>

// QPainter is the class allowing low-level drawing on the GUI elements
#include <QPainter>

// QImage is the class providing independent of hardware representations of the image.
// Allows direct access to individual pixels,
// We will use it for creating and storing our pictures
#include <QImage>

// The QMouseEvent class contains parameters that describe a mouse event.
// Mouse events occur when a mouse button is pressed
// or released inside a widget, or when the mouse cursor is moved.
#include <QMouseEvent>

#include <QColor>

#include <queue>

#include "point.h"
#include "beziercurve.h"
#include "bspline.h"
#include "polygon.h"

namespace Ui {
    class MyWindow;
}

enum class MyWindowMode {CIRCLE, ELLIPSE, BEZIER, BEZIER_POINT_DRAG, B_SPLINE, B_SPLINE_POINT_DRAG, FILL, POLYGON};

// MyWindow is a subclass of QMainWindow class.
class MyWindow : public QMainWindow
{
    // The Q_OBJECT macro is mandatory for any class
    // that implements signals, slots or properties.
    Q_OBJECT

public:
    // A typical declaration of constructor in Qt.
    // In the case of our class 'parent' parameter indicates to null
    // because it is a top-level component
    explicit MyWindow(QWidget *parent = nullptr);

    // Declaration of destructor
    ~MyWindow();

private:
    // Qt Creator allows you to create GUI using a graphical wizard.
    // Interface components and their properties are then stored in an XML file 'class_name.ui'
    // Access to individual elements of the GUI is obtained by the variable "ui"
    Ui::MyWindow *ui;

    // 'img' is a field storing the image
    QImage *img;

    // width of the image
    int img_width;

    // height of the image
    int img_height;

    // coordinates of the upper left corner of the image
    int img_x0;
    int img_y0;

    // line to draw
    int start_x;
    int start_y;

    // modes
    bool circleMode;
    bool ellipseMode;
    MyWindowMode mode;

    // ellipse data
    int ellipseNop;

    // Bezier curve data
    std::vector<BezierCurve> curves;
    static const int BC_HANDLE_SIZE = 4;
    uint whichCurveDragged;
    uint whichPointDragged;

    // B-spline data
    BSpline *bSpline;

    // Flood fill data
    std::queue<Point> pixelQueue;
    QColor fillColor;

    // Polygon drawing data
    std::vector<Polygon> polygons;

    // Declarations of drawing functions
    void img_clean();
    void img_draw1();
    void img_draw2();

    void drawLine(int x0, int y0, int x1, int y1);
    void drawLine(Point &p0, Point &p1);
    void drawCircle(int x0, int y0, int r);
    void drawPixel(unsigned char *ptr, int draw_x, int draw_y, unsigned char red = 0xff, unsigned char green = 0xff, unsigned char blue = 0xff);
    void drawPixel(unsigned char *ptr, int draw_x, int draw_y, QColor color);
    void drawEllipse(int start_x, int start_y, int finish_x, int finish_y, int ellipseNop);
    void drawBezierCurve(BezierCurve &curve);
    void drawPointHandle(unsigned char *ptr, int draw_x, int draw_y);
    void drawBSpline(BSpline &bSpline);
    void floodFill(int x, int y);
    bool isPixelValid(int x, int y);
    QColor pixelColor(uchar *ptr, int x, int y);
    void drawPolygon(int x, int y);
    void scanLine();
private slots:
    // Declarations of slots
    // A slot is a function that is called in response to a particular event,
    // eg. event associated with the GUI (mouse move, button press, etc.)
    void on_draw2Button_clicked();
    void on_draw1Button_clicked();
    void on_cleanButton_clicked();
    void on_exitButton_clicked();
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent*);

    void mouseReleaseEvent(QMouseEvent *event);
    void on_radioCircle_toggled(bool checked);
    void on_radioCircle_clicked();
    void on_radioEllipse_toggled(bool checked);
    void on_ellipse_nop_input_valueChanged(int arg1);
    void on_radioBezier_toggled(bool checked);
    void on_radioBSpline_toggled(bool checked);
    void on_radioFill_toggled(bool checked);
    void on_pickColorButton_clicked();
    void on_radioPolygon_toggled(bool checked);
    void on_scanLineButton_clicked();
};

#endif // MYWINDOW_H
