#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T00:14:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lines
TEMPLATE = app


SOURCES += main.cpp\
        mywindow.cpp \
    point.cpp \
    beziercurve.cpp \
    bspline.cpp \
    polygon.cpp

HEADERS  += mywindow.h \
    point.h \
    beziercurve.h \
    bspline.h \
    polygon.h

FORMS    += mywindow.ui

QMAKE_CXXFLAGS += -std=c++11
