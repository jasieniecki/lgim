#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include <QImage>

#include "point.h"

class BezierCurve
{
public:
    BezierCurve(int size_x, int size_y);
    Point& point(int index);
    bool complete();
    void clean();
    bool drawn();
    void addPoint(Point p);
    QImage& getCanvas();
private:
    static const int max_points = 4;
    Point points[max_points];
    bool complete_value;
    bool drawn_value;
    int number_of_points_value;
    QImage image_value;
};


#endif // BEZIERCURVE_H
