#include "beziercurve.h"


BezierCurve::BezierCurve(int size_x, int size_y)
    :
      complete_value(false),
      drawn_value(false),
      number_of_points_value(0),
      image_value(QSize(size_x, size_y), QImage::Format_ARGB32)
{
    clean();
}

Point& BezierCurve::point(int index)
{
    if (index < 0 or index >= max_points)
    {
        throw new std::out_of_range("Index out of bounds");
    }
    return points[index];
}

bool BezierCurve::complete()
{
    return complete_value;
}

void BezierCurve::clean()
{
    int size_x = image_value.height();
    int size_y = image_value.width();
    unsigned char *ptr = image_value.bits();
    for (int i = 0; i < size_x; ++i)
    {
        for (int j = 0; j < size_y; ++j)
        {
            for (int c = 0; c < 4; ++c)
            {
                ptr[size_y * j * 4 + i * 4 + c] = 0x00;
            }
        }
    }
}

bool BezierCurve::drawn()
{
    return drawn_value;
}

void BezierCurve::addPoint(Point p)
{
    if (complete_value)
    {
        throw new std::range_error("Curve complete");
    }
    points[number_of_points_value++] = p;
    if (number_of_points_value == max_points)
        complete_value = true;
}

QImage& BezierCurve::getCanvas()
{
    return image_value;
}

