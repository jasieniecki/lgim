#include <stdexcept>

#include "polygon.h"

Point& Polygon::point(unsigned long index)
{
    if (index < points.size())
        return points[index];
    else
        throw std::exception();
}

unsigned long Polygon::numberOfPoints()
{
    return points.size();
}

void Polygon::addPoint(Point p)
{
    points.emplace_back(p);
}

void Polygon::setComplete()
{
    isComplete = true;
}

bool Polygon::complete()
{
    return isComplete;
}
