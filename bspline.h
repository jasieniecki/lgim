#ifndef BSPLINE_H
#define BSPLINE_H

#include <vector>

#include <QImage>

#include "point.h"

class BSpline
{
public:
    BSpline(int size_x, int size_y);
    Point& point(unsigned index);
    unsigned numberOfPoints();
    void clean();
    void addPoint(Point p);
    QImage& getCanvas();
private:
    static const int degree = 4;
    std::vector<Point> points;
    QImage image_value;
};

#endif // BSPLINE_H
