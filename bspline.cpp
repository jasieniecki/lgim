#include "bspline.h"

BSpline::BSpline(int size_x, int size_y)
    :
      image_value(QSize(size_x, size_y), QImage::Format_ARGB32)
{}

Point& BSpline::point(unsigned index)
{
    if (points.size() <= index)
        throw new std::out_of_range("Index out of bounds");
    return points[index];
}

unsigned BSpline::numberOfPoints()
{
    return points.size();
}

void BSpline::clean()
{
    int size_x = image_value.height();
    int size_y = image_value.width();
    unsigned char *ptr = image_value.bits();
    for (int i = 0; i < size_x; ++i)
    {
        for (int j = 0; j < size_y; ++j)
        {
            for (int c = 0; c < 4; ++c)
            {
                ptr[size_y * j * 4 + i * 4 + c] = 0x00;
            }
        }
    }
}

void BSpline::addPoint(Point p)
{
    points.push_back(p);
}

QImage& BSpline::getCanvas()
{
    return image_value;
}
