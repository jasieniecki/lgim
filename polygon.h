#ifndef POLYGON_H
#define POLYGON_H

#include <QImage>

#include "point.h"

class Polygon
{
public:
    Point& point(unsigned long index);
    unsigned long numberOfPoints();
    void addPoint(Point p);
    void setComplete();
    bool complete();
private:
    std::vector<Point> points;
    bool isComplete;
};

#endif // POLYGON_H
